const { app, BrowserWindow, Menu } = require('electron')
const shell = require('electron').shell
const fs = require('fs')
const root = fs.readdirSync('/')

// Храните глобальную ссылку на объект окна, если вы этого не сделаете, окно будет
// автоматически закрываться, когда объект JavaScript собирает мусор.
let win

function createWindow () {
  // Создаёт окно браузера.
    win = new BrowserWindow({ width: 800, height: 600 })

    // и загрузит index.html приложение.
    win.loadFile('src/index.html')

    // Открыть средства разработчика.
    win.webContents.openDevTools()
    console.log(root)
    // Вызывается, когда окно будет закрыто.
    win.on('closed', () => {
    // Разбирает объект окна, обычно вы можете хранить окна     
    // в массиве, если ваше приложение поддерживает несколько окон в это время,
    // тогда вы должны удалить соответствующий элемент.
    win = null
    })

    var menu= Menu.buildFromTemplate([
        {
            label: 'Menu',
            submenu: [
                {
                    label: 'Open site in browser', 
                    click(){
                        shell.openExternal('http://rule34hentai.net')
                    }
                },
                {
                    type: 'separator'
                },
                {
                    label: 'Exit', 
                    click(){
                        app.quit()
                    }
                }
            ]
        }  
    ])
    Menu.setApplicationMenu(menu);
}



// Этот метод будет вызываться, когда Electron закончит 
// инициализацию и готов к созданию окон браузера.
// Некоторые интерфейсы API могут использоваться только после возникновения этого события.
app.on('ready', createWindow)

// Выйти, когда все окна будут закрыты.
app.on('window-all-closed', () => {
  // Оставаться активным до тех пор, пока пользователь не выйдет полностью с помощью Cmd + Q,
  // это обычное дело для приложений и их строки меню на macOS
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
   // На MacOS обычно пересоздают окно в приложении,
   // после того, как на иконку в доке нажали, и других открытых окон нету.
  if (win === null) {
    createWindow()
  }
})